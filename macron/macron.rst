.. index::
   ! Macron

.. _macron_2023:

=========================================
|macron| 🤥macron🤥
=========================================


.. _macron_casserolle_2023:

Avec une casserolle sur la tête
===================================

.. figure:: macron_avec_casserolle_sur_la_tete.jpeg
   :align: center
   :width: 500

   https://france.attac.org/se-mobiliser/retraites-pour-le-droit-a-une-retraite-digne-et-heureuse/article/on-ne-les-lache-pas-la-carte-des-mobilisations


.. _macron_chef_casserolles:

Macron chef des casserolles
===================================

.. figure:: macron_et_ses_casserolles.jpeg
   :align: center
   :width: 500


.. _macron_petain_2023:

Macron pétain
===================================

.. figure:: macron_petain.jpeg
   :align: center
   :width: 500

.. _macron_chemin_demo_2023:

Le chemin démocratique vu par Macron |macron| (bla bla)
============================================================


.. figure:: democratie_1.png
   :align: center
   :width: 500

Le chemin démocratique vu par Macron |macron| (la réalité)
===============================================================

.. figure:: democratie_2.png
   :align: center
   :width: 500

Macron clown sinistre
========================

.. figure:: macron_clown_sinistre.jpg
   :align: center
   :width: 500

🤥 Macron menteur masques 🤥
==============================

.. figure:: macron_menteur_masques.jpeg
   :align: center
   :width: 500


Avril 2023
===========

.. toctree::
   :maxdepth: 3

   04/04
